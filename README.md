#  Readme for a Snakemake mapping RNA-Seq reads to the modified A.thaliana genome

Scholastica Quaicoe

This git contains all the steps required to map RNA-Seq reads to
the genome fasta file using Snakemake.



## Step 1) Install minconda on lustre/mnt


```
Create conda environment
    > conda create --name env_name
    > conda config --add channels bioconda
    > conda config --add channels conda-forge
    
    > conda activate env_name
    
    Install tools with deired versions
    > conda install fastqc=0.11.9
    > conda install hisat2=2.2.1
    > conda install samtools=1.7
    > conda install stringtie=2.2.1
   
    
    Note: accept installation of additional packages
    
    Exit conda environment
    > conda deactivate

```

## Step 2) Run the snakefile
 ```
    > snakemake -s Snake_ldl3 --cores 5   

 ```
Pipeline runs these steps:

Generate list of all raw read files in Raw Data

Create Index genome

Map reads with hisat2

Sort and index sam files with samtools -> bam files

Stringtie generates counts per gene/transcript

Merging Stringtie gtf files

Gather all gtf files

Run prepDE.py using all gtf filenames
